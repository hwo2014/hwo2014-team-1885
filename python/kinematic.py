import math

a_multiplier = 0.2
b = 0.02


def get_acceleration(throttle):
    return a_multiplier * throttle


def get_distance_from_velocity(vi, vf, throttle,x=None,b=None):
    # TODO fix vi,vf, a,b later a/b should be the maximum velocity
    if x is None:
        x = 0.2
    if b is None:
        b=0.02
    a = x*throttle
    # if vi > vf:
    #     vi,vf = vf, vi
    s = 1 / b * (vi - vf - a / b * math.log((vf - a / b) / (vi - a / b)))
    return s
