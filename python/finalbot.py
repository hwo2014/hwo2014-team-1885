import json
import logging
import socket
import sys
import math

import kinematic as k
import bot_util as b


Left = 'Left'
Right = 'Right'


class Bot(object):
    def __init__(self, socket, name, key):
        self.braking = False
        self.socket = socket
        self.name = name
        self.key = key
        self.throttle_value = 1
        self.race = b.Race(self.name)
        self.estimator = b.Estimator()
        self.has_switch_lane = False
        self.prev_sector = 0
        self.prev_lap = -1
        self.braking_distance = 0
        self.turbo_available = False
        self.slip_speed = 6
        self.is_set_slip_speed = False
        self.ctr_crash = 0
        self.turtle_throttle = 0.5

    def msg(self, msg_type, data, gametick=None):
        if gametick:
            self.send(json.dumps({"msgType": msg_type, "data": data, "gameTick": gametick}))
        else:
            self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        logging.debug('[out]\t {}\t{}'.format(str(self.race.tic), msg))
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle, gametick=None):
        if gametick:
            self.msg("throttle", throttle, gametick)
        else:
            self.msg("throttle", throttle)

    def ping(self):
        self.msg("ping", {})

    def join_track(self, name):
        return self.msg("joinRace", {"botId": {"name": self.name, "key": self.key}, "trackName": name, "carCount": 1})

    def run(self, name=None):
        if name:
            self.join_track(name)
        else:
            self.join()
        self.msg_loop()

    def on_join(self, msg):
        logging.info("Joined")

    def on_game_init(self, msg):
        logging.info("Game Init")
        logging.debug('gameinit {}'.format(msg))
        self.race.loads(msg)

    def switch_lane(self, direction):
        self.msg("switchLane", direction)

    def on_game_start(self, msg):
        logging.info("Race started")
        self.throttle(self.throttle_value)

    def run_turbo(self, message="MiawMiaw Power Up!"):
        self.msg("turbo", message)

    def decide_action(self):


        logging.debug('br {}, ctr {} lap cur {} prev {} slip_speed {}'.format(self.braking, self.ctr_crash, self.race.mycar().lap, self.prev_lap, self.slip_speed))
        if self.race.mysector() != self.prev_sector:
            self.has_switch_lane = False
            if self.race.mysector().type == b.Sector.BEND \
                and not self.is_set_slip_speed \
                and self.race.mycar().slip_angle > 0:
                self.is_set_slip_speed = True
                self.slip_speed = max(self.race.mycar().speed, 2)

        logging.debug("{},{}".format(self.estimator.max_curve_speed, self.race.get_next_curve_radius(self.race.mycar().id_sector)))
        if self.race.mycar().lap != self.prev_lap:
            source = (0, self.race.mycar().lane_start)
            self.race.shortest_path = self.race.find_shortest_path(source)


            self.estimator.max_curve_speed = self.slip_speed/math.sqrt(120)*math.sqrt(self.race.get_next_curve_radius(self.race.mycar().id_sector))

        if self.ctr_crash > 3:
            throttle_value = ((self.slip_speed/10.0)+ self.turtle_throttle) /2.0 # turtle_mode
        elif self.race.mysector().type == b.Sector.STRAIGHT:
            if self.race.mycar().speed > self.estimator.max_curve_speed:
                self.braking_distance = k.get_distance_from_velocity(self.race.mycar().speed,
                                                                     self.estimator.max_curve_speed,
                                                                     0,
                                                                     self.estimator.x,
                                                                     self.estimator.b)
            dist_to_curve = self.race.track.get_distance_to_next_curve(self.race.mycar().id_sector,
                                                                       self.race.mycar().in_sector_distance) + 15
            logging.debug('dist : {:.2f} {:.2f}'.format(self.braking_distance, dist_to_curve))
            if self.race.mycar().speed > self.estimator.max_curve_speed:
                if (abs(self.braking_distance - dist_to_curve) < 5 or self.braking_distance > dist_to_curve):
                    throttle_value = 0
                    self.braking = True
                elif self.braking:
                    throttle_value = 0
                else:
                    throttle_value = 1
            elif self.race.mycar().speed < 1:
                throttle_value = self.slip_speed / 10
            else:
                throttle_value = self.throttle_value

        else:
            remaining_angle = self.race.track.get_angle_to_next_straight(self.race.mycar().id_sector,
                                                                         self.race.mycar().in_sector_distance)
            if remaining_angle < 23 or self.race.mycar().speed < 5:
                self.braking = False
                throttle_value = 1
            elif self.race.mycar().slip_angle < 20 and self.braking:
                if abs(self.race.mycar().omega) < 0.5 and remaining_angle < 80:
                    self.braking = False
                    throttle_value = self.estimator.max_curve_speed / 10
                else:
                    self.braking = False
                    throttle_value = self.estimator.max_curve_speed / 10
            elif self.braking:
                throttle_value = 0
            else:
                self.braking = False
                throttle_value = self.estimator.max_curve_speed / 10

        throttle_value = min(max(throttle_value,0), 1)
        # check if we can safely send lane change command
        if self.turbo_available and self.race.track.turbo_sector == self.race.mycar().id_sector:
            logging.info('===================TURBO ====================')
            self.run_turbo()
            self.turbo_available = False

        if abs(self.throttle_value - throttle_value) < 1e-2 and not self.has_switch_lane:
            # send other command
            current_id = self.race.mycar().id_sector
            n_sector = len(self.race.track.sectors)

            path = [v for x, v in enumerate(self.race.shortest_path) if
                    v[0] in {(current_id + 1) % n_sector, (current_id + 2) % n_sector}]
            # logging.debug('shpath {}'.format(self.race.shortest_path))
            # logging.debug('id: {} path {}'.format(current_id, path))
            # if in the next sector the lane changes, send switch lane command
            if path[1][0] == (current_id + 1) % n_sector:
                before = path[0]
                after = path[1]
            else:
                before = path[1]
                after = path[0]

            if before[1] > after[1]:
                logging.info('\n\nchange to left lane')
                self.switch_lane(Left)
            elif before[1] < after[1]:
                logging.info('\n\nchange to right lane')
                self.switch_lane(Right)
            else:
                self.ping()
            self.has_switch_lane = True

        else:
            self.throttle_value = throttle_value
            self.throttle(throttle_value, self.race.tic)

        logging.info('throttle: {:.2f}'.format(self.throttle_value))
        self.prev_sector = self.race.mysector()
        self.prev_lap = self.race.mycar().lap

    def on_car_positions(self, msg):
        logging.info('Update car position : qual={}'.format(self.race.is_qualification))
        self.race.update_state(msg)

        if msg.has_key('gameTick'):
            if self.race.tic > 50:
                self.decide_action()
            else:
                self.throttle(1)
                self.estimator.estimate(self.race.mycar(), self.throttle_value)
                logging.debug(self.estimator)


    def on_crash(self, msg):
        self.ctr_crash += 1
        self.braking = False
        self.race.mycar().speed = 0
        self.throttle_value = self.slip_speed/10.0
        if self.ctr_crash > 4:
            self.turtle_throttle -= 0.1
        logging.info("====================Someone crashed==================")


    def on_lap_finished(self, msg):
        pass


    def on_game_end(self, msg):
        logging.info("Race ended")
        if self.race.is_qualification:
            self.race.is_qualification = False
        else:
            self.race.is_qualification = False


    def on_error(self, msg):
        logging.info("Error: {0}".format(msg))


    def on_spawn(self, msg):
        self.throttle(self.slip_speed/10, self.race.tic)


    def on_turbo_available(self, msg):
        self.turbo_available = True


    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'lapFinished': self.on_lap_finished,
            'turboAvailable': self.on_turbo_available,
        }

        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            logging.debug('[in]\t{}'.format(line))
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](msg)
            else:
                logging.info("Got {0}".format(msg_type))
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) < 5:
        print("Usage: ./run host port botname botkey [trackname]")
    else:
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.DEBUG)
        host, port, name, key = sys.argv[1:5]
        print(host, port, name, key)
        if len(sys.argv) == 6:
            track_name = sys.argv[5]
        else:
            track_name = None
        print(track_name)
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Bot(s, name, key)
        bot.run(track_name)