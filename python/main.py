import socket
import sys
import logging

import finalbot as bot


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = bot.Bot(s, name, key)
        bot.run()
