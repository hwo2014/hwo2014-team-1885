from __future__ import print_function
import json
import sys
import math
import logging as log
import operator

try:
    import networkx as nx
except:
    log.error('no networkx')

import numpy as np


class Estimator(object):
    def __init__(self):
        self.vs = [0]
        self.vmax = 0
        self.k = 0
        self.m = 0
        self.b = 0
        self.x = 0
        self.done = False
        self.max_curve_speed = 6

    def estimate(self, car, throttle):
        self.vs.append(car.speed)
        if len(self.vs) == 3:
            self.k = (self.vs[1] - ( self.vs[2] - self.vs[1])) \
                     / self.vs[1] ** 2 * throttle
            log.debug(self.k)
            log.debug(self.vs)
            log.debug((self.vs[2] - (throttle / self.k)) / (self.vs[1] - (throttle / self.k)))
            self.m = 1.0 / (math.log((self.vs[2] - (throttle / self.k))
                                     / (self.vs[1] - (throttle / self.k))) / (-self.k))
            self.b = self.k / self.m
            self.x = throttle / self.m
            self.vmax = 1 / self.k
            self.done = True

    def __str__(self):
        return "vmax: {}, k: {}, m: {} x:{} b:{}".format(self.vmax, self.k, self.m, self.x, self.b)

    def __repr__(self):
        return self.__str__()


class Race(object):
    QUALIFYING = 0
    FINAL = 1

    def __init__(self, mycar_name):
        self.mybot_name = mycar_name
        self.mycar_id = 'red'
        # self.mycar = None
        self.cars = {}
        self.track = None
        self.game_id = 0
        self.duration = 0
        self.tic = 0
        self.is_qualification = True
        self.g = nx.DiGraph()
        self.shortest_path = None

    def mycar(self):
        return self.cars[self.mycar_id]

    def loads(self, json_str):
        log.info('load gameinit json')
        log.debug(isinstance(json_str, str))
        if isinstance(json_str, str):
            msg = json.loads(json_str)
        else:
            msg = json_str

        race = msg['data']['race']
        self.load_cars(race['cars'])
        self.load_track(race['track'])
        self.game_id = msg['gameId']
        if race.has_key('raceSession') \
                and race['raceSession'].has_key('durationMs'):
            self.duration = race['raceSession']['durationMs']
        source = (0,self.mycar().lane_start)
        #initialize shortest path using current lane
        self.shortest_path = self.find_shortest_path(source)

    def load_cars(self, cars):
        log.info('loading cars')
        self.cars = {}
        for car in cars:
            if car['id']['name'] == self.mybot_name:
                self.mycar_id = car['id']['color']
            self.cars[car['id']['color']] = Car(car['id']['name'],
                                                car['id']['color'],
                                                car['dimensions']['length'],
                                                car['dimensions']['width'],
                                                car['dimensions']['guideFlagPosition'])

    def load_track(self, track, initial_position=np.array([0, 0]), offset_angle=0):
        log.info('loading track')
        # get starting point
        if track.has_key('startingPoint'):
            starting_position = np.array([track['startingPoint']['position']['x'],
                                          track['startingPoint']['position']['y']])
            starting_angle = track['startingPoint']['angle']
        else:
            starting_position = np.array([0, 0])
            starting_angle = 0

        self.track = Track(track['id'], starting_position, starting_angle)
        lanes = track['lanes']

        # load the lanes
        for lane in lanes:
            self.track.add_lane(lane['index'], lane['distanceFromCenter'])

        # load the sectors
        track_pieces = track['pieces']
        pos_init = starting_position
        offset_angle = 0
        for sector in track_pieces:
            if sector.has_key('switch'):
                is_switch_lane = sector['switch']
            else:
                is_switch_lane = False
            if sector.has_key('length'):
                # pos_init = pos_init + np.array([math.cos(math.pi / 180 * offset_angle),
                #                                 math.sin(math.pi / 180 * offset_angle)]) * sector['length']
                self.track.add_straight_sector(sector['length'],
                                               offset_angle, is_switch_lane, pos_init)

            elif sector.has_key('radius') and sector.has_key('angle'):
                # c = np.array([-math.sin(math.pi / 180 * offset_angle),
                #               math.cos(math.pi / 180 * offset_angle)]) * sector['radius']
                # pangle = sector['angle']
                # if sector['angle'] < 0:
                #     c = -c
                #     pangle = sector['angle'] + 180
                #
                # pos_init = pos_init + c + np.array([math.sin(math.pi / 180 * (offset_angle + pangle)),
                #                                     -math.cos(math.pi / 180 * (offset_angle + pangle))]) * sector[
                #                'radius']

                self.track.add_bend_sector(sector['radius'],
                                           sector['angle'],
                                           offset_angle,
                                           is_switch_lane, pos_init)
                offset_angle = offset_angle + sector['angle']
            else:
                log.debug('unknown track piece')

            pos_init = self.track.sectors[-1].pos_end

        self.create_graph()
        list_straight_sector = self.track.get_list_straight_sectors()
        if list_straight_sector:
            self.track.turbo_sector = list_straight_sector[0][0]

    def create_graph(self):
        self.g = nx.DiGraph()
        pos_init = self.track.starting_position
        #create nodes
        for sid, sector in enumerate(self.track.sectors):
            for lid, distance in self.track.lanes.iteritems():
                theta_start = sector.offset_angle - 90
                pos = sector.pos_init + (distance) * np.array(
                    [math.cos(math.pi / 180 * theta_start), math.sin(math.pi / 180 * theta_start)])
                self.g.add_node((sid, lid), {'pos': pos,
                                             'theta': theta_start,
                                             'lane': lid,
                                             'dist': distance})
        # add special nodes at the end, which are copies of the nodes in first sector
        for lid, distance in self.track.lanes.iteritems():
            theta_start = sector.offset_angle - 90
            pos = sector.pos_init + (distance) * np.array(
                [math.cos(math.pi / 180 * theta_start), math.sin(math.pi / 180 * theta_start)])
            self.g.add_node((sid + 1, lid), {'pos': pos,
                                             'theta': theta_start,
                                             'lane': lid,
                                             'dist': distance})

        # create edges
        for sid, sector in enumerate(self.track.sectors):
            if sector.is_switch_lane:
                min_lid = min(self.track.lanes.iterkeys())
                max_lid = max(self.track.lanes.iterkeys())
                for lid in self.track.lanes.iterkeys():
                    u = self.g.node[(sid, lid)]
                    v = self.g.node[(sid + 1, lid)]
                    if sector.type == Sector.STRAIGHT:
                        length = sector.length
                    else:
                        length = abs((sector.radius + u['dist']) * sector.angle * math.pi / 180)
                    self.g.add_edge((sid, lid), (sid + 1, lid), {'dist': length})

                    if lid > min_lid:
                        v = self.g.node[(sid + 1, lid - 1)]
                        self.g.add_edge((sid, lid), (sid + 1, lid - 1), {'dist': np.linalg.norm(u['pos'] - v['pos'])})
                    if lid < max_lid:
                        v = self.g.node[(sid + 1, lid + 1)]
                        self.g.add_edge((sid, lid), (sid + 1, lid + 1), {'dist': np.linalg.norm(u['pos'] - v['pos'])})

            else:
                for lid in self.track.lanes.iterkeys():
                    u = self.g.node[(sid, lid)]
                    v = self.g.node[(sid + 1, lid)]
                    self.g.add_edge((sid, lid), (sid + 1, lid), {'dist': np.linalg.norm(u['pos'] - v['pos'])})

    def find_shortest_path(self, source):
        targets = [n for n in self.g.nodes() if n[0] == len(self.track.sectors)]
        min_distance = float('inf')
        min_target = None
        for target in targets:
            try:
                p = nx.shortest_path_length(self.g, source, target, weight='dist')
                # print(p, target)
                if p < min_distance:
                    min_distance = p
                    min_target = target
            except:
                log.debug('no path')
        if min_target is not None:
            # print(p, min_target, nx.shortest_path(self.g, source, min_target, weight='dist'))
            return nx.shortest_path(self.g, source, min_target, weight='dist')
        else:
            return None

    def get_next_curve_radius(self, sid):
        n_sector = len(self.track.sectors)
        temp = [x for x in xrange(sid + 1, n_sector + sid) if (not self.track.is_straight_sector(x))]
        if temp:
            bend_id = (temp[0]) % n_sector

            lane_id = [y for x,y in self.shortest_path if x == bend_id]

            radius = self.track.sectors[bend_id].radius + self.track.lanes[lane_id[0]]
            print(bend_id, lane_id,self.track.sectors[bend_id].radius ,self.track.lanes[lane_id[0]] )
            return radius
        else:
            return 50


    def update_state(self, msg):
        log.info('updating car state')
        if msg.has_key('gameTick'):
            self.tic = msg['gameTick']
        cars = msg['data']
        for car in cars:
            id_sector = car['piecePosition']['pieceIndex']
            lap = car['piecePosition']['lap']
            in_sector_distance = car['piecePosition']['inPieceDistance']
            slip_angle = car['angle']
            lane_start = car['piecePosition']['lane']['startLaneIndex']
            lane_end = car['piecePosition']['lane']['endLaneIndex']
            self.cars[car['id']['color']].update(id_sector,
                                                 lap,
                                                 in_sector_distance,
                                                 slip_angle,
                                                 lane_start,
                                                 lane_end)

        log.debug(self.mycar())


    def get_sector(self, sector_id):
        return self.track.sectors[sector_id]


    def mysector(self):
        return self.track.sectors[self.mycar().id_sector]


    def update_car_parameters(self, b, x, k, m):
        for car in self.cars:
            car.update_parameter(b, x, k, m)


    def __str__(self):
        output = "Race : id {}\nmycar: {}\nopponent\n{}\n {}".format(self.game_id,
                                                                     self.mycar,
                                                                     self.opponents,
                                                                     self.track)
        return output


    def __repr__(self):
        return self.__str__()


class Car(object):
    def __init__(self, name, color, length, width, guide_flag_position):
        self.name = name
        self.color = color
        self.length = length
        self.width = width
        self.guide_flag_position = guide_flag_position
        self.position = np.array([0, 0])

        # car parameters, need to be estimated
        self.b = 0.02
        self.x = 0.2
        self.k = 0.1
        self.m = 5

        # state
        self.id_sector = 0
        self.in_sector_distance = 0
        self.slip_angle = 0
        self.lane_start = 0
        self.lane_end = 0
        self.lap = 0
        self.speed = 0
        self.prev_speed = 0
        self.omega = 0
        self.prev_omega = 0
        # self.prev_slip_angle = 0

    def update_parameter(self, b=0.02, x=0.2, k=0.1, m=5):
        self.b = b
        self.x = x
        self.k = k
        self.m = m

    def update(self, id_sector, lap, in_sector_distance, slip_angle, lane_start, lane_end):
        #update delta before update state
        self.omega = slip_angle - self.slip_angle
        if self.in_sector_distance < in_sector_distance:
            # only update speed when we don't change sector
            self.speed = (in_sector_distance - self.in_sector_distance)

        self.id_sector = id_sector
        self.in_sector_distance = in_sector_distance
        self.slip_angle = slip_angle
        self.lane_start = lane_start
        self.lane_end = lane_end
        self.lap = lap
        self.omega_dot = self.omega - self.prev_omega
        self.prev_omega = self.omega

    def velocity(self, vi, throttle, dt):
        return (vi - (throttle * self.x / self.b)) * math.exp(-self.b * dt) \
               + (throttle * self.x / self.b)


    def __str__(self):
        return "name: {}, color: {}, " \
               "dim: L{} W{} G{} pos: {}, " \
               "lap: {}, sec:{}, v:{}, agl:{}, " \
               "lane:{} {}".format(self.name,
                                   self.color,
                                   self.length,
                                   self.width,
                                   self.guide_flag_position,
                                   self.position,
                                   self.lap,
                                   self.id_sector,
                                   self.speed,
                                   self.slip_angle,
                                   self.lane_start,
                                   self.lane_end)

    def __repr__(self):
        return self.__str__()


class Sector(object):
    STRAIGHT = 0
    BEND = 1

    def __init__(self, is_switch_lane=False):
        self.is_switch_lane = is_switch_lane
        #     if sector_type == STRAIGHT:
        #     	self.length = args


class StraightSector(Sector):
    def __init__(self, length, offset_angle=0, is_switch_lane=False, pos=np.array([0, 0])):
        Sector.__init__(self, is_switch_lane)
        self.length = length
        self.type = Sector.STRAIGHT
        self.angle = 0
        self.radius = float('Inf')
        self.offset_angle = offset_angle
        self.pos_init = pos
        self.pos_end = self.pos_init + np.array([math.cos(math.pi / 180 * self.offset_angle),
                                                 math.sin(math.pi / 180 * self.offset_angle)]) * self.length


    def __str__(self):
        return "[{}, 0, {}, {}]".format(self.type, self.length, int(self.is_switch_lane))

    def __repr__(self):
        return self.__str__()


class BendSector(Sector):
    def __init__(self, radius, angle, offset_angle=0, is_switch_lane=False, pos=np.array([0, 0])):
        Sector.__init__(self, is_switch_lane)
        self.radius = radius
        self.angle = angle
        self.type = Sector.BEND
        # TODO this is approximate, sector length depent of the actual radius
        # the radius given here is the center of the sector
        self.length = abs(self.radius * self.angle * math.pi / 180)
        self.offset_angle = offset_angle
        self.pos_init = pos
        c = np.array([-math.sin(math.pi / 180 * self.offset_angle),
                      math.cos(math.pi / 180 * self.offset_angle)]) * self.radius
        pangle = self.angle
        if self.angle < 0:
            c = -c
            pangle = self.angle + 180
        self.pos_end = self.pos_init + c + np.array([math.sin(math.pi / 180 * (self.offset_angle + pangle)),
                                                     -math.cos(
                                                         math.pi / 180 * (self.offset_angle + pangle))]) * self.radius

    def __str__(self):
        return "[{}, {}, {}, {}]".format(self.type, self.radius,
                                         self.angle, int(self.is_switch_lane))

    def __repr__(self):
        return self.__str__()


class Track(object):
    def __init__(self, id=None, pos=(0, 0), angle=0):
        self.sectors = list()
        self.width = 0
        self.lanes = {}
        self.id = id
        self.starting_position = pos
        self.starting_angle = angle
        self.turbo_sector = -1

    def add_lane(self, id, distance):
        self.lanes[id] = distance
        # self.lanes.add(distance)
        self.update_width()

    def update_width(self):
        if len(self.lanes) == 1:
            self.width = max(self.lanes.iteritems(), key=operator.itemgetter(1))[1]
        else:
            self.width = max(self.lanes.iteritems(), key=operator.itemgetter(1))[1] \
                         - min(self.lanes.iteritems(), key=operator.itemgetter(1))[1]

    def add_straight_sector(self, length, offset_angle, is_switch, pos=np.array([0, 0])):
        self.sectors.append(StraightSector(length, offset_angle, is_switch, pos))

    def add_bend_sector(self, radius, angle, offset_angle, is_switch_lane, pos=np.array([0, 0])):
        self.sectors.append(BendSector(radius, angle, offset_angle, is_switch_lane, pos))

    def load_track(self, data):
        if data.has_key('race') and data['race'].has_key('track'):
            track_pieces = data['race']['track']['pieces']
            # load the track pieces
            offset_angle = 0
            for sector in track_pieces:

                if sector.has_key('switch'):
                    is_switch_lane = sector['switch']
                else:
                    is_switch_lane = False
                if sector.has_key('length'):
                    self.add_straight_sector(sector['length'], offset_angle, is_switch_lane)
                elif sector.has_key('radius') and sector.has_key('angle'):
                    self.add_bend_sector(sector['radius'], sector['angle'], offset_angle, is_switch_lane)
                    offset_angle = offset_angle + sector['angle']
                else:
                    log.error('unknown track piece')
            # load the lanes
            if data['race']['track'].has_key('lanes'):
                lanes = data['race']['track']['lanes']
                for lane in lanes:
                    if lane.has_key('distanceFromCenter'):
                        self.add_lane(lane['index'],lane['distanceFromCenter'])

    def get_next_bend_sector_strict(self, sid):
        n_sector = len(self.sectors)

        temp = [x for x in xrange(sid + 1, n_sector + sid) if
                (not self.is_straight_sector(x))]
        if temp:
            return (temp[0]) % n_sector

    def get_list_straight_sectors(self):
        n_sector = len(self.sectors)

        temp_list = list()
        first_bend = self.get_next_bend_sector_strict(0)
        start_bend = first_bend
        while True:
            start_straight = self.get_next_straight_sector(start_bend)
            end_straight = self.get_next_bend_sector_strict(start_straight) - 1
            if end_straight >= start_straight:
                len_straight = sum(
                [x.length for i, x in enumerate(self.sectors) if start_straight <= i <= end_straight])
            else:
                len_straight = sum(
                [x.length for i, x in enumerate(self.sectors) if i>=start_straight or i <= end_straight])

            temp_list.append((start_straight, end_straight, len_straight))
            start_bend = end_straight + 1

            if start_bend == first_bend:
                break
        return sorted(temp_list, key=lambda straight: straight[2],reverse=True)

    def is_straight_sector(self, id):
        """ return true if the sector with the id is a straight sector
            return false otherwise. id can be larger that the number
            of sector, it will loop back to the lowest id.
            i.e. id = id % len(self.sector)
        :param id:
        :return:
        """
        id %= len(self.sectors)
        return self.sectors[id].type == Sector.STRAIGHT

    def get_next_bend_sector(self, sid):
        n_sector = len(self.sectors)

        temp = [x for x in xrange(sid + 1, n_sector + sid) if
                (not self.is_straight_sector(x) and abs(self.sectors[x % n_sector].angle) > 25)]
        if temp:
            return (temp[0]) % n_sector
            # print [x for x in xrange(sid + 1, n_sector + sid) if (not self.is_straight_sector(x)) and self.sectors[x%n_sector].angle>30]
            # bend_id = next((x for x in xrange(sid + 1, n_sector + sid) if (not self.is_straight_sector(x)) and self.sectors[x%n_sector].angle>30), [-1])
            # return bend_id%n_sector

    def get_next_straight_sector(self, sid):
        n_sector = len(self.sectors)

        temp = [x for x in xrange(sid + 1, n_sector + sid) if
                (self.is_straight_sector(x))]
        if temp:
            return (temp[0]) % n_sector

    def get_distance_to_next_curve(self, current_id, distance_in_sector):
        next_bend_sector_id = self.get_next_bend_sector(current_id)
        remaining_distance = max(self.sectors[current_id].length - distance_in_sector, 0)
        if next_bend_sector_id > current_id + 1:
            # we know that the next sector is straight
            distance = remaining_distance + sum(
                [x.length for i, x in enumerate(self.sectors) if current_id < i < next_bend_sector_id])
        elif next_bend_sector_id < current_id:
            distance = remaining_distance + sum(
                [x.length for i, x in enumerate(self.sectors) if i > current_id or i < next_bend_sector_id])
        elif next_bend_sector_id == current_id + 1:
            distance = remaining_distance
        else:
            distance = 10000
        return distance

    def get_next_different_curve_sector(self, sid):
        n_sector = len(self.sectors)
        temp = [x for x in xrange(sid + 1, n_sector + sid) if
                (not self.is_straight_sector(x)
                 and math.copysign(1, self.sectors[x % n_sector].angle) != math.copysign(1, self.sectors[sid%n_sector].angle))]
        if temp:
            return (temp[0]) % n_sector

    def get_angle_to_next_straight(self, current_id, distance_in_sector):
        n_sector = len(self.sectors)
        next_straight_sector_id = self.get_next_straight_sector(current_id)
        next_diff_curve_id = self.get_next_different_curve_sector(current_id)

        if next_diff_curve_id :
            if next_diff_curve_id < current_id:
                next_diff_curve_id += n_sector

            next_straight_sector_id = min(next_straight_sector_id, next_diff_curve_id) % n_sector
        current_angle = abs(180 / math.pi * distance_in_sector / self.sectors[current_id].radius)
        remaining_angle = abs(self.sectors[current_id].angle) - current_angle
        if next_straight_sector_id > current_id + 1:
            # we know that the next sector is straight
            angle = remaining_angle + sum(
                [abs(x.angle) for i, x in enumerate(self.sectors) if current_id < i < next_straight_sector_id])
        elif next_straight_sector_id < current_id:
            angle = remaining_angle + sum(
                [abs(x.angle) for i, x in enumerate(self.sectors) if i > current_id or i < next_straight_sector_id])
        elif next_straight_sector_id == current_id + 1:
            angle = remaining_angle
        else:
            angle = 360

        return angle

    def load_track_from_json(self, json_str):
        msg = json.loads(json_str)
        if msg.has_key('data'):
            data = msg['data']
            self.load_track(data)

    def export_track_to_matlab(self, track_json=None):
        for sector in self.sectors:
            if sector.type:
                print("[{}, {}, deg2rad({}), {}]".format(sector.type, sector.radius,
                                                         -sector.angle, int(sector.is_switch_lane)))
            else:
                print("[{}, 0, {}, {}]".format(sector.type, sector.length, int(sector.is_switch_lane)))

    def __str__(self):
        out = "track \n=====\n width: {}\n lanes: {}\n sectors:{}\n".format(self.width, self.lanes, self.sectors)
        return out

    def __repr__(self):
        return self.__str__()


def sector_id(msg):
    return msg[0]['piecePosition']['pieceIndex']


def game_tick(msg):
    # if type(msg)==list:
    #     msg = msg[0]
    # print(msg)
    # if msg.has_key('gameTick'):
    #     return msg['gameTick']
    if msg.has_key('gameTick'):
        return msg['gameTick']


def parse_log_old(filename):
    file = open(filename, 'r')
    lines = file.readlines()
    for log in lines:
        if '[in]' in log:
            log_type, log_json = log.split('\t', 2)
            msg = json.loads(log_json)
            if msg['msgType'] == 'carPositions' and msg.has_key('gameTick'):
                print(msg['gameTick'],
                      msg['data'][0]['piecePosition']['lap'],
                      msg['data'][0]['piecePosition']['pieceIndex'],
                      msg['data'][0]['piecePosition']['lane']['startLaneIndex'],
                      msg['data'][0]['piecePosition']['lane']['endLaneIndex'],
                      msg['data'][0]['piecePosition']['inPieceDistance'], )
            elif msg['msgType'] == 'finish':
                break


def parse_log(filename):
    file = open(filename, 'r')
    line = file.readline()
    msgs = json.loads(line)
    current_tick = 0
    out = ''
    track = Track()
    for msg in msgs:
        if msg['msgType'] == 'gameInit':
            track.load_track(msg['data'])
        elif msg['msgType'] == 'fullCarPositions' and msg.has_key('gameTick'):
            current_tick = msg['gameTick']
            data = [msg['gameTick'],
                    msg['data'][0]['piecePosition']['lap'],
                    msg['data'][0]['piecePosition']['pieceIndex'],
                    msg['data'][0]['piecePosition']['lane']['startLaneIndex'],
                    msg['data'][0]['piecePosition']['lane']['endLaneIndex'],
                    msg['data'][0]['piecePosition']['inPieceDistance'],
                    msg['data'][0]['coordinates']['x'],
                    msg['data'][0]['coordinates']['y'],
                    int(track.is_straight_sector(msg['data'][0]['piecePosition']['pieceIndex'])),
                    msg['data'][0]['angle'],
                    msg['data'][0]['angleOffset']
            ]
            out = [str(x) for x in data]
        elif msg['msgType'] == 'carVelocities' and msg.has_key('gameTick'):
            assert (current_tick == msg['gameTick'])
            data = [msg['data'][0]['x'], msg['data'][0]['y']]
            out.extend([str(x) for x in data])
            print('\t'.join(out))
            out = ''
        elif msg['msgType'] == 'finish':
            break


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print('usage : {} logfilename'.format(sys.argv[0]))
        sys.exit(1)
    parse_log(sys.argv[1])
    # export_track_to_matlab()